-- AOC 2018 Day 5
module Y2018D05 where

import Data.Char

main :: IO ()
main = do
    input <- readFile "./inputs/2018/05.txt"
    print "Part 1"
    print $ part1 . parseInput $ input
    print "Part 2"
    print $ part2 . parseInput $ input

parseInput :: String -> String
parseInput = head . words

part1 :: String -> Int
part1 = length . react

-- Remove adjacent mixed case recursively
react :: String -> String
react = foldr go ""
    where
        -- chars not equal, but equal when same case? react. (Remove them)
        go x (y:ys) | x /= y && toUpper x == toUpper y = ys
        go x ys                                        = x : ys

-- Remove all instances of a character (upper/lower) from a string
removeAll :: Char -> String -> String
removeAll c = filter (/= toUpper c) . filter (/= toLower c)

part2 :: String -> Int
part2 str = minimum lengths
    where lengths = [part1 (removeAll c str) | c <- ['a'..'z']]
