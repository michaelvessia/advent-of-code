-- AOC 2018 Day 1
module Y2018D01 where

import Data.Maybe (fromJust)
import qualified Data.Set as Set

main :: IO ()
main = do
    input <- readFile "./inputs/2018/01.txt"
    print (sum $ parseInput input)
    print (part2 $ parseInput input)


-- Given file input as string
-- Turn each line into an int
parseInput :: String -> [Integer]
parseInput = map readInt . lines

-- Parse a string representation of an int into an Integer
readInt :: String -> Integer
readInt ('+':xs) = read xs
readInt xs = read xs

-- Find the first duplicate in a list
firstDuplicate :: Ord a => [a] -> Maybe a
firstDuplicate = go Set.empty
    -- if empty list, no duplicate
    where go _ [] = Nothing
          go seen (x:xs)
              -- if x is in the set, return it
              | x `Set.member` seen = Just x
              -- otherwise, add x to seen map and recur
              | otherwise = go (Set.insert x seen) xs

-- Given list, compute partial sums
-- [1,1,1] => [0,1,2,3]
-- [1,-2,3] => [0,1,-1,2]
-- [] => [0]
partialSums :: Num a => [a] -> [a]
partialSums = scanl (+) 0

-- Given list of freqs, find the first partial sum that repeats
-- [1,-1] => 0
-- [3,3,4,-2,-4] => 10
-- [-6,3,8,5,-6] => 5
-- [7,7,-2,-7,-4] => 14
part2 :: [Integer] -> Integer
part2 = fromJust . firstDuplicate . partialSums . cycle
