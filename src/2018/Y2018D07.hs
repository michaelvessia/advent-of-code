-- AOC 2018 Day 7
module Y2018D07 where

import qualified Data.Map as Map
import Data.List.Split
import Data.Char

type Step = Char

testInput = "Step C must be finished before step A can begin.\nStep C must be finished before step F can begin.\nStep A must be finished before step B can begin.\nStep A must be finished before step D can begin.\nStep B must be finished before step E can begin.\nStep D must be finished before step E can begin.\nStep F must be finished before step E can begin."

main :: IO ()
main = do
    input <- readFile "./inputs/2018/07.txt"
    print "Part 1"
    let steps = parseInput . lines $ input
    print steps
    print $ Map.fromList steps
    print "Part 2"

parseInput :: [String] -> [(Step,Step)]
parseInput = map getPair
    where getPair xs = (head (words xs !! 1), head (words xs !! 7))

