-- AOC 2018 Day 3
module Y2018D03 where

import Data.List.Split
import Data.Map (Map)
import qualified Data.Map as Map

type Id = Int
type Position = (Int, Int)
type Dimension = (Int, Int)
data Claim = Claim Id Position Dimension deriving (Show, Eq)

type Coord = (Int, Int)

p1Test = "#1 @ 1,3: 4x4\n#2 @ 3,1: 4x4\n#3 @ 5,5: 2x2"

main :: IO ()
main = do
    input <- readFile "./inputs/2018/03.txt"
    print "Part 1"
    print $ part1 . overlap $ parseInput input
    print "Part 2"
    let claims = parseInput input
    let m = overlap claims
    print $ part2 m claims
    

-- How many square inches of fabric or within two or more claims?
part1 :: Map Coord Int -> Int
part1 = Map.foldl' (\acc x -> if (>1) x then acc+1 else acc) 0

-- What is the id of the only claim with no overlap?
part2 :: Map Coord Int -> [Claim] -> Id
part2 fabric claims = head [getId claim
                               | claim <- claims
                               , all (== 1) (allIntersections fabric claim)]

-- #1 @ 1,3: 4x4 => Claim 1 (1,3) (4,4)
-- #2 @ 3,1: 4x4 => Claim 2 (3,1) (4,4)
-- #3 @ 5,5: 2x2 => Claim 3 (5,5) (2,2)
parseInput :: String -> [Claim]
parseInput input = map processLine (lines input)
    where processLine line = Claim id' pos' dim'
              where parts = splitOneOf "#@,:x " line
                    id' = read $ parts !! 1
                    pos' = (read $ parts !! 4, read $ parts !! 5)
                    dim' = (read $ parts !! 7, read $ parts !! 8)

-- Get all of the coordinates that a claim covers
coverage :: Claim -> [Coord]
coverage (Claim _ (x, y) (l, h)) = [(a,b) 
                             | a <- [x .. (x + l - 1)]
                             , b <- [y .. (y + h - 1)]
                         ]

-- Get Coordinates mapped to how many times they occur in the given claims
overlap :: [Claim] -> Map Coord Int
overlap = occurences . concatMap coverage

-- Get a map from element -> occurences given a list
occurences :: Ord a => [a] -> Map a Int
occurences xs = Map.fromListWith (+) [(x, 1) | x <- xs]


-- Get all intersections of the current fabric and a given Claim
allIntersections :: Map Coord Int -> Claim -> Map Coord Int
allIntersections m1 c = Map.intersection m1 (overlap [c])

getId :: Claim -> Id
getId (Claim i _ _) = i
