-- AOC 2018 Day 2
module Y2018D02 where

import Data.List

p2Test = ["abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz"]

main :: IO ()
main = do
    input <- readFile "./inputs/2018/02.txt"
    print "Part 1"
    print (part1 $ lines input)
    print "Part 2"
    putStrLn (part2 $ lines input)

part1 :: [String] -> Int
part1 lines = doubles * triples
    where doubles = sum $ map (getNs 2 . getOccurences) lines
          triples = sum $ map (getNs 3 . getOccurences) lines

-- Take string and map to tuple of (char, occurences)
getOccurences :: String -> [(Char, Int)]
getOccurences = map (\xs -> (head xs, length xs)) . group . sort

-- 1 if there is a character which occured n times, else 0
getNs :: Int -> [(Char, Int)] -> Int
getNs n [(_, x)] = if x == n then 1 else 0
getNs n xs = if n `elem` map snd xs then 1 else 0


-- Get common elements in list that differ by one position
offByOne :: Eq a => [a] -> [a] -> Maybe [a]
offByOne (x:xs) (y:ys)
    | x == y = (x :) <$> offByOne xs ys
    | xs == ys = Just xs
offByOne _ _ = Nothing

-- Using list comprehension, find match and get offByOne
part2 :: [String] -> String
part2 s = head [r | x:xs <- tails s, y <- xs, Just r <- [offByOne x y]]
