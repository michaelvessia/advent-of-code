-- AOC 2018 Day 6
module Y2018D06 where

import Data.List.Split

type X = Int
type Y = Int
type Coord = (X, Y)
type Origin = (X, Y)

testCoords = [(1, 1),
             (1, 6),
             (8, 3),
             (3, 4),
             (5, 5),
             (8, 9)]

main :: IO ()
main = do
    input <- readFile "./inputs/2018/06.txt"
    --let coords = parseInput input
    let coords = testCoords
    print "Part 1"
    let maxX = maximum (map fst coords)
    let maxY = maximum (map snd coords)
    let minX = minimum (map fst coords)
    let minY = minimum (map snd coords)
    let grid = genGrid (minX, minY) (maxX, maxY)
    print grid
    print "Part 2"

parseCoord :: String -> Coord
parseCoord str = (x, y)
    where x = read . head . parts $ str
          y = read . head . parts $ str
          parts = splitOneOf ", "

parseInput :: String -> [Origin]
parseInput xs = map parseCoord (lines xs)

-- Take the sum of the absolute values of the differences of the coordinates.
manhattan :: Coord -> Coord -> Int
manhattan (a, b) (c, d) = abs (a-c) + abs (b - d)

-- Generate a grid from max X to max Y [(0, 0)..(X, 0)..Coord..(X, 0)]
genGrid :: Coord -> Coord -> [Coord]
genGrid (xl, yl) (xh, yh) = [(x,y) | x <- [xl..xh], y <- [yl..yh]]

neighbors :: Coord -> [Coord]
neighbors (x, y) = [(x+1, y), (x-1, y), (x, y-1), (x, y+1)]

closestOrigin :: Coord -> [Origin] -> Maybe Origin
closestOrigin (x,y) os = undefined
    

