-- AOC 2018 Day 4
module Y2018D04 where

import Data.List.Split
import Data.List
import qualified Data.Map as Map
import Data.Maybe (fromJust, isJust)

type Id = Int
type Minute = Int
data Event = Start Id | Sleep Minute | Wake Minute deriving (Show, Eq)

main :: IO ()
main = do
    input <- readFile "./inputs/2018/04.txt"
    print "Part 1"
    let events = parseInput $ sort . lines $ input
    let sleepsByGuard = getAllShifts events
    let allSleeps = Map.fromListWith (++) sleepsByGuard
    let totals = Map.toList $ fmap length allSleeps
    -- The laziest guard is the one with the longest list of sleep minutes
    let laziestId = fst $ maximumBy (\(_,x) (_,y) -> compare x y) totals
    let mostTiringMinute = fromJust $ mostCommon $ fromJust $ Map.lookup laziestId allSleeps
    --print laziestId
    --print mostTiringMinute
    print $ laziestId * mostTiringMinute
    print "Part 2"
    let minutes = guardMostCommonMinutes allSleeps
    let occurences = guardMostCommonMinuteWithOccurences allSleeps
    let guard = maximumBy (\(_,x) (_,y) -> compare x y) occurences
    print $ (fst guard) * (snd . head $ [x | x <- minutes, (fst x) == (fst guard)])


-- 1518-11-01 00:00] Guard #10 begins shift
-- [1518-11-01 00:05] falls asleep
-- [1518-11-01 00:25] wakes up
parseInput :: [String] -> [Event]
parseInput = map event where
  event l
    | isStartOfShift = Start (read $ tokens !! 7)
    | isFallingAsleep = Sleep (read $ tokens !! 3)
    | isWakingUp = Wake (read $ tokens !! 3)
    | otherwise = error "Invalid line?"
    where
      tokens = splitOneOf "[]:# " l
      isStartOfShift = tokens !! 5 == "Guard"
      isFallingAsleep = tokens !! 5 == "falls"
      isWakingUp = tokens !! 5 == "wakes"

newShift :: Event -> Bool
newShift (Start _) = True
newShift _ = False

guardId :: Event -> Id
guardId (Start x) = x
guardId _ = undefined

getAllShifts :: [Event] -> [(Id, [Minute])]
getAllShifts [] = []
getAllShifts xs = 
    -- Get the first shift
    let first = getShift xs
        -- Get the rest of the shifts, by dropping off any events
        -- from the first shift and recursively calling getAllShifts
        rest = getAllShifts (dropWhile (not . newShift) (tail xs))
    in (first:rest)

-- Given list of events
-- Return (Id, Minutes slept) for the first guard
getShift :: [Event] -> (Id, [Minute])
getShift xs = 
    let guard = guardId (head xs)
        shift = takeWhile (not . newShift) (tail xs)
        minsAsleep = calcAsleep shift
    in (guard, minsAsleep)

calcAsleep :: [Event] -> [Minute]
calcAsleep [] = []
calcAsleep ((Sleep m1):(Wake m2):xs) = [m1..m2-1] ++ calcAsleep xs

sleepiestGuard :: [(Id, Minute)] -> (Id, Minute)
sleepiestGuard [g] = g
sleepiestGuard (g:g':gs) | snd g >= snd g' = sleepiestGuard (g:gs)
                         | otherwise = sleepiestGuard (g':gs)

-- Get the most common element in a list
mostCommon :: Ord a => [a] -> Maybe a
mostCommon [] = Nothing
mostCommon xs = Just (snd . maximum . map (\xs -> (length xs, head xs)) . group . sort $ xs)

-- Get the number of occurences of the most common element in a list
mostCommonOccurences :: Ord a => [a] -> Maybe Int
mostCommonOccurences [] = Nothing
mostCommonOccurences xs = Just (fst . maximum . map (\xs -> (length xs, head xs)) . group . sort $ xs)

guardMostCommonMinutes :: Map.Map Id [Minute] -> [(Id, Minute)]
guardMostCommonMinutes allSleeps = getOnlyJusts $ Map.toList $ fmap mostCommon allSleeps

guardMostCommonMinuteWithOccurences :: Map.Map Id [Minute] -> [(Id, Int)]
guardMostCommonMinuteWithOccurences allSleeps = getOnlyJusts $ Map.toList $ fmap mostCommonOccurences allSleeps

getOnlyJusts xs = map (fmap fromJust) $ filter (\(_,j) -> isJust j) xs

