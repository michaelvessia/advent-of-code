-- AOC 2018 Day 8
module Y2018D08 where

-- Input is space delimited ints
parseInput :: String -> [Int]
parseInput = map read . words

main :: IO ()
main = do
    file <- readFile "./inputs/2018/08.txt"
    let input = parseInput file
    print "Part 1"
    print input
    print "Part 2"
