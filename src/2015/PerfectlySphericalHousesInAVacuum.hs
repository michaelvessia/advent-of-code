-- 2015 Day 3
module PerfectlySphericalHousesInAVacuum where

import Data.List

type Position = (Integer, Integer)

main :: IO ()
main = do
  input <- readFile "./inputs/2015/03.txt"
  print $ part1 input


-- Count (x, y) pairs that are visted at least once
-- ^>v< => 4
-- ^v^v^v^v^v => 2
part1 :: String -> Int
part1 s = countUniques (visited moves)
  where moves = map getMove s

-- Get move from arrow
getMove :: Char -> Position
getMove c
  | c == '^' = (0, 1)
  | c == 'v' = (0, -1)
  | c == '>' = (1, 0)
  | c == '<' = (-1, 0)
  | otherwise = (0, 0)

move :: Position -> Position -> Position
move (x1, y1) (x2, y2) = (x1+x2, y1+y2)

countUniques :: [Position] -> Int
countUniques = length . nub

visited :: [Position] -> [Position]
visited = scanl move (0, 0)

splitDirections :: String -> (String, String)
splitDirections [] = ([], [])
splitDirections [x] = ([x], [])
splitDirections (x:y:xs) = ([x], [y]) -- not complete

