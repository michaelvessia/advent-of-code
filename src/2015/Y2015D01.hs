-- AOC 2015 Day 1
module Y2015D01 where

import Data.List
import Data.Maybe(fromJust)

main :: IO ()
main = do
  input <- readFile "./inputs/2015/01.txt"
  print $ part1 input
  print $ part2 input

-- What is the sum of a string of parens where ( => +1 and ) => -1?
-- (()) => 0
-- ((( => 3
-- (()(()( => 3
-- ()) => -1
part1 :: String -> Integer
part1 s = sum $ parensToDigits s

parensToDigits :: String -> [Integer]
parensToDigits [] = []
parensToDigits "(" = [1]
parensToDigits ")" = [-1]
parensToDigits (x:xs) = upOrDown x : parensToDigits xs
  where upOrDown s
         | s == '(' = 1
         | s == ')' = -1
               | otherwise = 0


-- What index first gives a negative number?
-- ) => 1
-- ()()) => 5
part2 :: String -> Int
part2 s = fromJust $ elemIndex (-1) (scanl (+) 0 (parensToDigits s))


