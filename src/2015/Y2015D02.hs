-- AOC 2015 Day 2
module Y2015D02 where

import Data.List
import Data.List.Split

type Dimensions = (Integer, Integer, Integer)

main :: IO ()
main = do
  input <- readFile "./inputs/2015/02.txt"
  print $ part1 (parseInput input)
  print $ part2 (parseInput input)

-- Given file string. return list of tuples representing (L, W, H)
parseInput :: String -> [Dimensions]
parseInput input = map getDimensions (lines input)

getDimensions :: String -> Dimensions
getDimensions s = toTriple (map read $ splitOn "x" s)
  where toTriple [l,w,h] = (l, w, h)

-- 2*l*w + 2*w*h + 2*h*l
getSurfaceArea :: Dimensions -> Integer
getSurfaceArea (l, w, h) = (2 * l * w) + (2 * w * h) + (2 * h * l)

-- area of the smallest side
getSlack :: Dimensions -> Integer
getSlack (l, w, h) = s1 * s2
  where [s1, s2] = take 2 $ sort [l, w, h]


-- Given list of rectangle dimensions, what is total surface area, plus 1 foot slack
part1 :: [Dimensions] -> Integer
part1 dims = sum $ zipWith (+) (map getSlack dims) (map getSurfaceArea dims)

-- Total feet of ribbons
-- Smallest perimeters of any one face + volume
part2 :: [Dimensions] -> Integer
part2 dims = sum $ zipWith (+) (map smallestPerimeter dims) (map volume dims)

perimeter :: (Integer, Integer) -> Integer
perimeter (s1, s2) = 2 * s1 + 2 * s2

smallestPerimeter :: Dimensions -> Integer
smallestPerimeter (l, w, h) = perimeter (s1, s2)
  where (s1, s2) = toTuple $ take 2 $ sort [l, w, h]
        toTuple [x, y] = (x, y)

volume :: Dimensions -> Integer
volume (l, w, h) = l * w * h
